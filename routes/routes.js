const express = require('express');
const router = express.Router();

const controller = require(`./../controllers/controller`)

//get one task
router.get(`/:taskId`, async (req,res)=>{
    try{
        await controller.getOneTask(req.params.taskId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err.message)
    }
})

module.exports = router

//create tasks list
router.post(`/new`,async (req,res)=>{
    try{
        await controller.createTask(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err.message)
    } 
})

//get all tasks
router.get(`/`,async (req,res)=>{
    try{
        await controller.getTasks().then(result =>res.send(result))
    }catch(err){
        res.status(500).json(err.message)
    }
})

//update one tasks
router.put(`/:taskId`, async(req,res) => {
    try{
       await controller.updateTask(req.params.taskId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err.message)
    }
})

//delete one tasks
router.delete(`/delete/:taskId`, async (req,res)=>{
    try{
        await controller.deleteTask(req.params.taskId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err.message)
    }
})