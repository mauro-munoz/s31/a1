const myTask = require(`./../models/model`)

//Create new task

module.exports.createTask = async (reqBody) => {

    return await myTask.findOne({taskname: reqBody.taskname}).then((result,err)=>{
        if(result != null && result.taskname == reqBody.taskname){
            return `Task is already registered`
        }else {
            let newTask = new myTask({
                "taskname": reqBody.taskname,
                "status": reqBody.status
            })
            newTask.save()
            return (`${reqBody.taskname} was successfully registered`)
        }
    })
}


//Find Specific task
module.exports.getOneTask = async(reqParams) => {
    return await myTask.findById(reqParams).then((result,err)=>{
        if (result != null){
            return result
        }else {
            return `Task ${reqParams} cannot be found`
        }
    })
}

//get all taskS
module.exports.getTasks = async () => {
    //return the result to route where function getUsers was called
   return await myTask.find({}).then((result,err)=>{
        // console.log(typeof result)
        return result
    })
}

//update one task
module.exports.updateTask = async (id, reqBody) => {
    //new:false returns Old document before update, true returns New document
        return await myTask.findByIdAndUpdate(id, {$set: reqBody}, {new:false}).then(result =>result)
    }

//delete task
module.exports.deleteTask = async (id) => {
    //new:false returns Old document before update, true returns New document
        return await myTask.findByIdAndRemove(id).then(result =>result)
    }