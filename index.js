const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const port = 3000;
const app = express();

//Middleware used to parse data from client
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Connect to database in MongoDB
mongoose.connect(process.env.MONGO_URL_TASKS, {useNewUrlParser: true, useUnifiedTopology: true})
//Test connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {console.log(`Connected to Database`)
});


const taskRouter = require('./routes/routes');
app.use(`/tasks`,taskRouter);

//listen to port in local hose
app.listen(port,()=>console.log(`Server is active at port ${port}`));