const mongoose = require('mongoose');


//Schema
const taskSchema = new mongoose.Schema({
    taskname: {
        type: String,
        required: [true,`Name is required`]
    },
    status: String
})
//Models
module.exports = mongoose.model('taskModel',taskSchema)
